# Installation

Clone via `git clone git@gitlab.com:nmalacarne/front_builder.git`

## Vagrant Setup

```
vagrant plugin install vagrant-vbguest
vagrant up
vagrant ssh
```

## Rails Setup

The `provision` script should take care of installing gem dependencies and
running the initial migrations, so you should be good to go after running
`vagrant up`.

```
cd front_builder
rails s -b 0.0.0.0 -p 8080
```

Visit `localhost:8080` to view the site.

# Contributing

Please read the [Contribution Guidelines](https://gitlab.com/nmalacarne/front_builder/blob/master/CONTRIBUTING.md).
