class CreatePasswordResets < ActiveRecord::Migration[5.1]
  def change
    create_table :password_resets do |t|
      t.string :token, :null => false
      t.belongs_to :user, :null => false
      t.timestamps
    end
  end
end
