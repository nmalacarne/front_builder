# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'faker'

50.times do
  User.create(
    email: Faker::Internet.unique.email, 
    password: 'secret'
  )
end

User.all.each do |user|
  user.adventure_fronts.create!(title: Faker::Book.title)
end

# create dev user

dev = User.create(
  email: ENV['DEV_EMAIL'] || 'front-builder@dev.com',
  password: ENV['DEV_PASSWORD'] || 'password'
)

50.times do
  dev.adventure_fronts.create(title: Faker::Book.title)
end
