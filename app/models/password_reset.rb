class PasswordReset < ApplicationRecord
  validates :token, presence: true

  before_validation :generate_token, on: [ :create ]

  belongs_to :user

  def to_param
    self.token
  end

  private

  def generate_token
    self.token = SecureRandom.uuid
  end
end
