class User < ApplicationRecord
  has_secure_password

  validates :email, presence: true
  validates :password, presence: true

  validates_confirmation_of :email
  validates_confirmation_of :password

  validates_presence_of :email_confirmation, 
    :if => :password_digest_changed?, 
    :on => :update

  has_many :password_resets
  has_many :adventure_fronts
end
