class UserSerializer < ActiveModel::Serializer
  attributes :id

  link(:self) { href user_url(object) }
end
