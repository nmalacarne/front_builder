class AdventureFrontSerializer < ActiveModel::Serializer
  attributes :id, :title

  belongs_to :user

  link(:self) { href adventure_front_url(object) }
  link(:user) { href user_url(object.user) }
end
