class PasswordMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.password_mailer.reset.subject
  #
  def reset(password_reset)
    @password_reset = password_reset
    mail to: password_reset.user.email
  end
end
