class ApplicationMailer < ActionMailer::Base
  default from: 'mailer@front-builder.com'
  layout 'mailer'
end
