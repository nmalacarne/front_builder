class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    false
  end

  def show?
    not record.nil?
  end

  def create?
    user.admin? or user.id == record.user_id  
  end

  def update?
    user.admin? or user.id == record.user_id  
  end

  def destroy?
    user.admin? or user.id == record.user_id  
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
