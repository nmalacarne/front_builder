class UsersController < ApplicationController
  def index
    head :not_implemented
  end

  def create
    User.create(user_create_params) 
    head :created
  end

  def show
    head :not_implemented
  end

  def update
    head :not_implemented
  end

  def destroy
    head :not_implemented
  end

  private

  def user_create_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
