class AdventureFrontsController < ApplicationController
  before_action :authenticate_user
  before_action :get_adventure_front, only: [ :show, :update, :destroy ]

  def index
    render json: AdventureFront.page(params[:page])
  end

  def create
    @adventure_front = AdventureFront.new(create_params)
    authorize @adventure_front
    @adventure_front.save
    head :no_content
  end

  def show
    authorize @adventure_front
    render json: @adventure_front
  end

  def update
    authorize @adventure_front
    @adventure_front.update(update_params)
    head :no_content
  end

  def destroy
    authorize @adventure_front
    @adventure_front.destroy
    head :no_content
  end

  private

  def create_params
    params.require(:adventure_front).permit(:title, :user_id)
  end

  def update_params
    params.require(:adventure_front).permit(:title)
  end

  def get_adventure_front
    @adventure_front = AdventureFront.find(params[:id])
  end
end
