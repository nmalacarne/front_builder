class ApplicationController < ActionController::API
  include Knock::Authenticable
  include Pundit

  rescue_from ActiveRecord::RecordNotFound, :with => :return_not_found
  rescue_from Pundit::NotAuthorizedError, :with => :return_forbidden

  def return_not_found
    head :not_found
  end

  def return_forbidden
    head :forbidden
  end
end
