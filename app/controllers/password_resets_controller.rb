class PasswordResetsController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, :with => :return_not_found

  before_action :get_user, only: [ :create ]
  before_action :get_password_reset, only: [ :update ]

  def index
    head :not_implemented
  end

  def create
    if @user
      password_reset = @user.password_resets.create
      PasswordMailer.reset(password_reset).deliver_now
    end

    head :accepted
  end

  def show
    head :not_implemented
  end

  def update
    if @password_reset.user.update(update_params)
      @password_reset.destroy
      head :no_content
    else
      render :json => { 
        :errors => @password_reset.user.errors.full_messages
      }, status: :bad_request
    end
  end

  def destroy
    head :not_implemented
  end

  private

  def create_params
    params.require(:user).permit(:email)
  end

  def update_params
    params.require(:user).permit(:email_confirmation, :password, :password_confirmation)
  end

  def get_user
    @user = User.find_by!(email: create_params[:email])
  end

  def get_password_reset
    @password_reset = PasswordReset.find_by!(token: params[:token])
  end
end
