require 'test_helper'

class AdventureFrontPolicyTest < ActiveSupport::TestCase
  setup :init_user
  setup :init_adventure_front

  setup do
    @policy = AdventureFrontPolicy.new(@user, @adventure_front)
  end

  def test_show
    assert @policy.show?
  end

  def test_create
    assert @policy.create?
  end

  def test_update
    assert @policy.update?
  end

  def test_destroy
    assert @policy.destroy?
  end
end
