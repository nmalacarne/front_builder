require 'test_helper'


class PasswordMailerTest < ActionMailer::TestCase
  setup :init_password_reset

  test "reset" do
    mail = PasswordMailer.reset(@password_reset)
    assert_equal "Front Builder Password Reset Link", mail.subject
    assert_equal ["mailer@front-builder.com"], mail.from
    assert_equal [@password_reset.user.email], mail.to
  end
end
