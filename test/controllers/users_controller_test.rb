require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
    test 'should create user' do
      assert_difference 'User.count', 1 do
        post users_path, params: {
          user: {
            email: 'test@test.com',
            password: 'secret',
            password_confirmation: 'secret'
          }  
        }
      end

      assert_response :created
    end
end
