require 'test_helper'

class UserTokenControllerTest < ActionDispatch::IntegrationTest
  setup :init_user 

  test "create jwt token" do
    post user_token_path, params: {
      auth: {
        email: @user.email,
        password: 'secret'
      }
    }

    assert_response :created
  end

  test "404 with invalid credentials" do
    post user_token_path, params: {
      auth: {
        email: 'noop',
        password: 'noop'
      }
    }

    assert_response :not_found
  end
end
