require 'test_helper'

class PasswordResetsControllerTest < ActionDispatch::IntegrationTest
  setup :init_user 
  setup :init_password_reset

  test 'should create password_reset record' do
    assert_difference 'PasswordReset.count', 1 do
      post password_resets_path, params: {
        user: {
            email: @user.email
        }
      } 
    end

    assert_response :accepted
  end

  test 'should send reset email' do
    mail = ActionMailer::Base.deliveries.clear
    assert_difference 'mail.count', 1 do
      post password_resets_path, params: {
        user: {
            email: @user.email
        }
      } 
    end
  end

  test 'should update password with valid email' do
    original = @user.clone
    put password_reset_path @password_reset, params: {
      user: {
        email_confirmation: original.email,
        password: 'new_password',
        password_confirmation: 'new_password'
      }
    }

    assert_response :no_content

    @user.reload
    assert_equal original.email, @user.email
    assert_not_equal original.password_digest, @user.password_digest

    assert_equal 0, PasswordReset.count
  end

  test 'should return not found with invalid email' do
    original = @user.clone
    put password_reset_path @password_reset, params: {
      user: {
        email: 'invalid_email',
        password: 'new_password',
        password_confirmation: 'new_password'
      }
    }

    assert_response :bad_request
    assert JSON.parse(@response.body).key?('errors') 

    @user.reload
    assert_equal original.email, @user.email
    assert_equal original.password_digest, @user.password_digest

    assert_equal 1, PasswordReset.count
  end

  test 'should return not found when token mismatch' do
    put password_reset_path 'fake_token'

    assert_response :not_found
  end

  test 'should return validation errors' do
    put password_reset_path @password_reset, params: {
      user: {
        password: '',
        password_confirmation: ''
      }
    }

    assert_response :bad_request
    assert JSON.parse(@response.body).key?('errors') 
  end
end
