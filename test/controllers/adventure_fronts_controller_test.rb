require 'test_helper'

class AdventureFrontsControllerTest < ActionDispatch::IntegrationTest
  setup :init_adventure_front

  test 'should show adventure front' do
    get adventure_front_path(@adventure_front), headers: auth_header

    assert_response :success
  end

  test 'should 404 with invalid id' do
    get adventure_front_path(0), headers: auth_header

    assert_response :not_found
  end

  test 'should destroy record' do
    delete adventure_front_path(@adventure_front), headers: auth_header 

    assert_response :no_content

    assert_equal 1, AdventureFront.count
  end

  test 'should create adventure front' do
    assert_difference 'AdventureFront.count' do
      post adventure_fronts_path, headers: auth_header, params: {
        adventure_front: {
          title: 'Testing',
          user_id: users(:active_user).id
        }
      }
    end

    assert_response :no_content
  end

  test 'should update adventure front' do
    original = @adventure_front.clone

    put adventure_front_path(@adventure_front), headers: auth_header, params: {
      adventure_front: {
        title: 'Update Title'
      }
    }

    assert_response :no_content

    assert_not_equal @adventure_front.reload.title, original.title
  end

  test 'should return paginated list of adventure fronts' do
    get adventure_fronts_path, headers: auth_header

    assert_response :success

    json = JSON.parse(@response.body)
    assert json.key?('data')
    assert json.key?('links')
  end
end
