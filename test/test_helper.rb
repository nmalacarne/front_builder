require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

# environments/test isnt being loaded for some reason
Rails.application.routes.default_url_options[:host] = '0.0.0.0:8080'
ActiveModelSerializers.config.adapter = :json_api

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  
  def init_user
    @user = users(:active_user)
  end

  def init_password_reset
    @password_reset = password_resets(:one)
  end

  def init_adventure_front
    @adventure_front = adventure_fronts(:one)
  end

  def auth_header
    token = Knock::AuthToken.new(payload: { sub: users(:active_user).id }).token

    {
      'Authorization': "Bearer #{token}"
    }
  end
end
