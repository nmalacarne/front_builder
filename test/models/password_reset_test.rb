require 'test_helper'

class PasswordResetTest < ActiveSupport::TestCase

  setup :init_user

  test 'should generate token on create' do
    password_reset = PasswordReset.create(user_id: @user)
    assert password_reset.token
  end

  test 'should not regenerate token on save' do
    password_reset = PasswordReset.create(user_id: @user.id)
    token = password_reset.token
    password_reset.save 
    assert_equal password_reset.token, token
  end
end
