require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "should be invalid with bad password confirmation" do
    user = User::new
    user.email = "testing@test.com"
    user.password = "testing"
    user.password_confirmation = "asfd"

    refute(user.valid?)
  end

  test "should require email" do
    user = User::new(password: 'testing')
    
    refute(user.valid?)
  end

  test "should require password" do
    user = User::new(email: 'testing@test.com')
    
    refute(user.valid?)
  end
end
