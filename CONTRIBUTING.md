# Issues

Branches that correspond with issues should follow the `issues/*` naming
convention. Commits to these branches should tag the issue that is being worked
on.

# Merge Requests

All work will be merged via merge (pull) requests. This allows team members to
code review changes. All merge requests must be approved by at least one other
project member (not including the author).

# Merging vs Rebasing

When a local branch falls behind its remote counterpart, we should rebase locally
instead of merging.

# Reference Material

[Automatic Issue Closing](https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html)

[Issue Overview](https://about.gitlab.com/2016/03/08/gitlab-tutorial-its-all-connected/)

[How to Create a Merge Request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)

[Merging vs. Rebasing] (https://www.atlassian.com/git/tutorials/merging-vs-rebasing)
